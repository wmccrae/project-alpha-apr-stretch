from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from .models import Project
from .forms import ProjectForm


# View the list of projects assigned to the logged in user.
@login_required
def project_list(request):
    list = Project.objects.filter(owner=request.user)
    context = {
        "projectlist": list,
    }
    return render(request, "projects/projectlist.html", context)


# Display the tasks for a specific project assigned to the logged in user
@login_required
def project_details(request, id):
    list = get_object_or_404(Project, id=id)
    context = {
        "tasklist": list,
    }
    return render(request, "projects/projectdetails.html", context)


# Create a new project.
# The 'login_required' decorator makes this view accessible only if
# the user is logged in.
@login_required
def create_project(request):
    if request.method == "POST":
        # We should use the form to validate the values
        #   and save them to the database
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            # If all goes well, we can redirect the browser
            #   to another page and leave the function
            return redirect("list_projects")
    else:
        # Create an instance of the Django model form class
        form = ProjectForm()
    # Put the form in the context
    context = {
        "form": form,
    }
    # Render the HTML template with the form
    return render(request, "projects/createproject.html", context)


# Allow the logged in user to edit the project. Return to the project detail page
# when complete.
@login_required
def edit_project(request, id):
    proj_to_edit = get_object_or_404(Project, id=id)
    if request.method == "POST":
        form = ProjectForm(request.POST, instance=proj_to_edit)
        if form.is_valid():
            form.save()
            return redirect("show_project", id)
    else:
        form = ProjectForm(instance=proj_to_edit)

    context = {
        "project_object": proj_to_edit,
        "project_form": form,
    }
    return render(request, "projects/editproject.html", context)
